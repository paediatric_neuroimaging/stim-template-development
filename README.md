# stim-template-development

## Please cite the following:
**The impact of premature extrauterine exposure on infants’ stimulus-evoked brain activity across multiple sensory systems.**  
Gabriela Schmidt Mellado, Kirubin Pillay, Eleri Adams, Ana Alarcon, Foteini Andritsou, Maria M Cobo, Ria Evans Fry, Sean Fitzgibbon, Fiona Moultrie, Luke Baxter, Rebeccah Slater.

## Project description
This project contains example code that underpins the development of new stimulus-specific templates from identifying the time window of interest using temporal cluster analysis to deriving the templates using Principal Component Analysis (PCA).  

### Code requirements
The template derivation is performed using MATLAB (R2018b or later) and the temporal clustering in Python (3.6 or later) in order to leverage the MNE Python package: https://mne.tools/stable/index.html.  

In addition, for specialised plotting in MATLAB, a full installation of the EEGLAB toolbox is required and all folders and subfolders should be on the MATLAB path. To install a copy of EEGLAB, visit: https://sccn.ucsd.edu/eeglab/download.php

### Workflow
To follow the workflow of the paper, the scripts should be read in the following order:  
1. **cluster_forpython.m**: Prepare the pre-processed EEG data in a format to be used with the python script (cluster_python.py)
2. **cluster_python.py**: Python script to perform temporal clustering of the data to identify significant time periods of evoked response activity
3. **derive_templates.m** & **derive_templates_noxious.m**: Derive candidate stimulus-specific templates using PCA
4. **project_templates.m**: Select and re-scale the template using control data (at term-age)

### Metadata
In addition to the MATLAB and Python scripts, some metadata is required to correctly plot and acquire additional information. The following files are templates and can be adjusted to the required EEG and data formats:    
- **adjacency_mat_25.xlsx**: Excel file identifying which channels are adjacent to one another for cluster analysis. This example is for a 25-channel arrangement.
- **eeg_placement.loc.txt**: EEGLAB-formatted text file with the EEG channel scalp coordinates. This examples is for a 25-channel arrangement.
- **data_demographics.xlsx**: Template for storing the supporting data for each patient such as age, latency etc.

## Installation
For running the MATLAB scripts, the project folder and subfolders can simply be added to your MATLAB path (ensure that EEGLAB is also on the same path).

For running the Python script, it is recommended to install the python dependencies into a virtual environment (such as venv or conda) with a Python >=3.6 interpreter. Once the virtual environment is activated and you are in the project path, simply run the following command to install additional required packages (MNE, NumPy etc.) before running the cluster_python.py script:  

`pip install -r requirements.txt`


