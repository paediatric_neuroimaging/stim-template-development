%{
Load and prepare data for use with python script 'cluster_python.py'
to perform clustering of the EEG data.

Each EEG data file is expected to consist of a .mat file containing an
array with dimensions (channels x timepoints). There is a seperate .mat
file assumed for the pre-stimulus and post-stimulus period and the data has
already been pre-processed (filtered, average-references, trial-averaged
etc.).

The following external files are also required:
 - an .xlsx file containing an adjacency matrix indicating which electrode channels are considered adjacent to one another for clustering.
(An example 25-channel file: 'adjacency_mat_25.xlsx' is provided in source
folder).

INPUTS are set below as follows:
filepath: filepath to data
patient: string of names for each patient (each patient file may contain multiple EEG recordings. This script assumes each recording)
rep_tot: maximum number of EEG files available per patient.
Fs: EEG sampling frequency
stim_type: v=visual, t=tactile, a=auditory 
chann_tot: Total number of channels in each EEG data file (must be the same)

NOTE: THIS CODE REQUIRES A FULL VERSION OF EEGLAB TO BE INSTALLED AND AVAILABLE ON THE PATH:
https://sccn.ucsd.edu/eeglab/download.php

Kirubin Pillay 
Department of Paediatrics, University of Oxford, Oxford, UK.
24/09/2019
%}

clear
close all

%% SET INPUTS
filepath='';
patient={};
rep_tot=4;
Fs=2000;
stim_type='t';
chann_tot=25;
[~,~,cont_data]=xlsread([filepath 'metadata/adjacency_mat_25.xlsx']);

switch stim_type
    case 'v'
        chann_max=23;           % Channel index to extract from data
    case 'pp'
        name='Noxious';
        chann_max=11;
    case 't'
        name='Tactile';
        chann_max=11;
    case 'a'
        name='Auditory';
        chann_max=9;
end

if chann_max>chann_tot
    error('Channel indices exceed total number of channel specified!');
end

%% INITIALIZE VARIABLES
%Empty data matrix
poststim_all=[];            %No. channels x time points x subjects
prestim_all=[];             %No. channels x time points x subjects

%% LOAD AND STORE PRE- AND POST- STIMULUS DATA
pat_tot=length(patient);
rep_count=0;
for pat_num=1:pat_tot
    pat_count=0;
    varname=patient{pat_num};
    for rep=1:rep_tot
        flag=exist([filepath '_' varname  '_' num2str(rep) '_' stim_type '.mat'], 'file');
        if flag==2
            rep_count=rep_count+1;
            pat_count=pat_count+1;
            
            %Load pre- and post- stimulus EEG
            load([filepath '_' varname  '_' num2str(rep) '_' stim_type '.mat'], 'EEG_pre_avg');     % Load pre-stimulus data
            load([filepath '_' varname  '_' num2str(rep) '_' stim_type '.mat'], 'EEG_post_avg');    % Load post-stimulus data      
            
            %Normalize using the pre-stimulus baseline
            EEG_pre_avg_cut=EEG_pre_avg(:,end-(Fs-1):end);                 %Take last 1000ms of pre-stim (just before stimulus)
            pre_mean=mean(EEG_pre_avg_cut, 2);
            pre_sd=std(EEG_pre_avg_cut,[],2);
            EEG_detrend_pre=(EEG_pre_avg_cut-pre_mean)./pre_sd;
            
            EEG_detrend_post=(EEG_post_avg-pre_mean)./pre_sd;
            EEG_detrend_post=EEG_detrend_post(:,1:Fs);                     %Take first 1000ms of post-stim (just after stimulus)
            
            %Store de-trended signal
            poststim_all=cat(3,poststim_all,EEG_detrend_post);
            prestim_all=cat(3,prestim_all,EEG_detrend_pre);
            
            clear EEG_pre_avg EEG_post_avg
        end
    end
end
%% Determine difference between prestim and poststim (for cluster analysis)
diffstim_all=poststim_all-prestim_all;

%% SAVE THE DATA FOR LOADING INTO PYTHON SCRIPT
save([filepath 'cluster_EEGdata_v1.0_' stim_type '.mat'],'prestim_all','poststim_all','diffstim_all','adj','-v7.3');
