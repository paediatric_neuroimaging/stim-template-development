%{
Derive template for NOXIOUS stimulus using Principal Component Analysis.
This  script assumes application of an experimental noxious stimulus (PinPrick) which may introduces apparatus-based delays 
and is corrected for in this script.

Each EEG file is assumed to consist of the format <filepath + patient name + EEG recording number + stimulus>. 
Each EEG data file is expected to consist of a .mat file containing an
array with dimensions (channels x timepoints). There is a seperate .mat
file assumed for the pre-stimulus and post-stimulus period and the data has
already been pre-processed (filtered, average-referenced, trial-averaged
etc.).

The following external files are also required:
 - an .xlsx demographic file containing demographic information for each
 patient. (An example file format: 'data_demographics.xlsx' is provided in source folder)

INPUTS are set below as follows:
filepath: filepath to data
patient: string of names for each patient (each patient file may contain multiple EEG recordings. This script assumes each recording)
control_file: Name of .xlsx file containing demographic information for each patient.
rep_tot: maximum number of EEG files available per patient.
Fs: EEG sampling frequency
stim_type: v=visual, t=tactile, a=auditory 
num_PC: Total number of principal components to determine.
chann_tot: Total number of channels in each EEG data file (must be the same)
delay: The delays (in s) of the pin prick apparatus requiring correction (set to 0 if no delay)
colormap: Matlab-formatted colormap for plotting spatial heatmaps.

NOTE: THIS CODE REQUIRES A FULL VERSION OF EEGLAB TO BE INSTALLED AND AVAILABLE ON THE PATH:
https://sccn.ucsd.edu/eeglab/download.php

Kirubin Pillay 
Department of Paediatrics, University of Oxford, Oxford, UK.
24/09/2019
%}

clear
close all

%% SET INPUTS %%
filepath='';
patient={}; 
control_file='metadata/data_demographics.xlsx';
rep_tot=5;   
Fs=2000;
stim_type='pp';
num_PC=3;
chann_tot=25;
delay=0.04;
colormap='jet';


%% SELECT THE REGION/WINDOW OF INTEREST
delay_ind=delay*Fs;
st_time=400;    % Stimulus start time of template
en_time=800;    % Stimulus end time of template
channels=11;    % Desired channel row number in data
lat_ind=16;     % Column index for each EEG file's latency information in .xlsx demographic file. 
PMA_ind = 4;    % Column index for each EEG file's patient Postmenstrual Age (PMA) in .xlsx demographic file. 

epoch_st_time=-0.5;
epoch_en_time=1.5;

if channels>chann_tot
    error('Channel indices exceed total number of channel specified!');
end

%% Create matrices and labels for all the data
%Empty data matrix
poststim_all=[];            %No channels x time points x repeats
poststim_all_pre=[];        %No channels x time points x repeats

%Stimuli labels
pat_labels=[];          %Indicates if recordings are from the same patient

%% Load and store controls for each stimuli
pat_tot=length(patient);
latencies_all=[];

%Load patient demographic data
[~,~,cont_data]=xlsread([filepath control_file]);

%Load and store
rep_count=0;
for pat_num=1:pat_tot
    pat_count=0;
    varname=patient{pat_num};
    for rep=1:rep_tot
        flag=exist([filepath '_' varname  '_' num2str(rep) '_' stim_type '.mat'], 'file');
        if flag==2
            rep_count=rep_count+1;
            pat_count=pat_count+1;
            
            %Load pre- and post- stimulus EEG
            load([filepath '_' varname  '_' num2str(rep) '_' stim_type '.mat'], 'EEG_pre_avg');     % Load pre-stimulus data
            load([filepath '_' varname  '_' num2str(rep) '_' stim_type '.mat'], 'EEG_post_avg');    % Load post-stimulus data                     
            
            %Normalize using the pre-stimulus baseline
            EEG_pre_avg_cut=EEG_pre_avg(:,end-(Fs/2-1)-delay_ind:end-delay_ind);                    %Take last 500ms of pre for de-meaning
            pre_mean=mean(EEG_pre_avg_cut, 2);
            pre_sd=std(EEG_pre_avg_cut,[],2);            
            EEG_detrend_pre_cut=(EEG_pre_avg_cut-pre_mean);

            
            EEG_post_avg=[EEG_pre_avg(:,end-delay_ind+1:end), EEG_post_avg(:,1:end-delay_ind)];
            EEG_detrend_post=(EEG_post_avg-pre_mean);
            EEG_detrend_post=EEG_detrend_post(:,1:Fs+(Fs/2));                                       %Take first 1500ms of post
            
            %Combine the pre- and post- stimulus together
            EEG_detrend=[EEG_detrend_pre_cut, EEG_detrend_post];                                    %Combine pre 500ms and post 1500ms
            
            %Store de-trended signal
            poststim_all=cat(3,poststim_all,EEG_detrend);
            
            % Store patient labels
            pat_labels=[pat_labels,pat_num];
            
            % Store PMAs from demographics file
            ind=find(strcmp(cont_data(:,1),varname));
            PMA_all(rep_count,1)=cont_data{ind+(rep-1),PMA_ind};

            % Store latencies from demographics file
            latencies_all = [latencies_all, cont_data{ind+(rep-1),lat_ind}];
        end
    end
end

%% RESHAPE THE DATA FOR PCA
poststim_pca_post=[];
for i=1:length(pat_labels)
    poststim_pca_post=[poststim_pca_post; poststim_all(:,:,i)];
end
EEG_avg=squeeze(mean(poststim_all,3));

%% Isolate region of interest
poststim_pca_post=poststim_pca_post(:,(st_time/1000-epoch_st_time)*Fs+1:(en_time/1000-epoch_st_time)*Fs);
EEG_avg_cut=squeeze(mean(poststim_all(:,(st_time/1000-epoch_st_time)*Fs+1:(en_time/1000-epoch_st_time)*Fs,:),3));
poststim_pca=poststim_pca_post;

%% APPLY PCA
[coeff,score,latent]=pca(poststim_pca');
coeff=real(coeff);
varexp=sum(latent(1:num_PC)/sum(latent)*100);
disp(['Variance explained by the first ' num2str(num_PC) ' principal components (%): ' num2str(varexp)])

%% Restructure coefficient weightings to original shape
coeff_red=coeff(:,1:num_PC);
coeff_restruct=[];
for i=1:length(pat_labels)
    coeff_restruct=cat(3,coeff_restruct,coeff_red((25*(i-1))+1:(25*(i-1))+1+24,:));
end

%Take mean of the post-stim coefficients
coeff_mean=mean(coeff_restruct,3);


%% PLOT SPATIAL MAPS FOR EACH COMPONENT AFTER THRESHOLDING
for i=1:num_PC
    weights=coeff_mean(:,i);
    figure('Name',['PC ' num2str(i)],'NumberTitle','off');
    subplot(2,1,1); brain_plot_function(weights,colormap,'off'); colorbar;
    title('Coefficients');
    caxis([-0.15 0.15]);    
end
plot_EEG_mat(score(:,1:num_PC)',Fs,'PCA components',100,5);

%% SAVE RAW COEFFICIENT DATA FOR PCS AND COMPILED DEMOGRAPHICS DATA
save([filepath 'raw_templates_' stim_type '_v1.0.mat'],'coeff','coeff_mean','score','varexp','poststim_pca','poststim_all','st_time','en_time');
save([filepath 'term_latencies_' stim_type '_v1.0.mat'],'PMA_all','latencies_all');
