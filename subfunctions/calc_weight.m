function [weights]=calc_weight(X_new,template)

% Function to calculate template weights (magnitude) in a new data set
% Caroline Hartley 2016, adapted by Kirubin Pillay 06/06/2019

% Input: 
%       X_new = matrix of new data for which the weights are calculated.
%               Note this data should only be for the time region of interest
%       
%       template = vector of the template.
%
%       Both should be in the format where rows are
%       trials/subjects and columns are time points. 
%       Sampling rate is 2000 Hz
%
% Output: weights = the magnitude of the nociceptive brain activity in the
%                   new data
%
% This function in it's original form is open source and the following
% additional paper should be cited:
%
% Hartley, C. et al. (2017) ?Nociceptive brain activity as a measure of 
% analgesic efficacy in infants?, Science Translational Medicine, 9(388),
% p. eaah6122.

if size(X_new,2)~=size(template,2)
    
    if size(X_new,1)==size(template,2)
        error('Data:Incorrectformat',...
            'Error: Check data format. \nEach column should be one time point, each row is a different signal.')
    else
    
        error('Data:Incorrectlength',...
        'Error: Check data length and sampling rate.')
    end
end


%Centre the matrix by removing the mean
X_new = X_new - repmat(mean(X_new,2),1,size(X_new,2));

%Calculate the singular value decompostion of the template

[~,E,V] = svd(template);

%Use the SVD to calculate the weights of the new data
weights = X_new*V(:,1)*E(1,1)^-1; 

