%{
Function to generate scalp spatial plotting map for EEG.

NOTE: THIS CODE REQUIRES A FULL VERSION OF EEGLAB TO BE INSTALLED AND AVAILABLE ON THE PATH:
https://sccn.ucsd.edu/eeglab/download.php

The following external files are also required:
 - an EEGLAB electrode file specifying coordinates of electrode position
 (See EEGLAB website for details). An example file 'eeg_placement.loc.txt'
 is provided in source for 25-channel arrangement.

INPUTS:    data_vec: contains the values to be plotted in each electrode
                     location
           colormap: choice of colormap to use
           conv: flag to apply alternate interpolation if data_vec
                 contains pvalues. 1: use conv, 0: otherwise

Kirubin Pillay 
Department of Paediatrics, University of Oxford, Oxford, UK.
24/09/2019
%}

function brain_plot_function(data_vec, colormap, conv)

electrode_file='metadata/eeg_placement.loc.txt';

filename=mfilename('fullpath');
[filepath,~,~] = fileparts(filename);
addpath(genpath(filepath));
cd(filepath)

EEG.chanlocs=readlocs([filepath '/' electrode_file],'filetype','loc');

topoplot(data_vec,EEG.chanlocs, 'colormap', colormap, 'conv', conv); 