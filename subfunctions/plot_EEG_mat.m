%{
This function plots multichannel EEG from .mat files in for inspection.

NOTE: THIS CODE REQUIRES A FULL VERSION OF EEGLAB TO BE INSTALLED AND AVAILABLE ON THE PATH:
https://sccn.ucsd.edu/eeglab/download.php

The following external files are also required:
 - an EEGLAB electrode file specifying coordinates of electrode position
 (See EEGLAB website for details). An example file 'eeg_placement.loc.txt'
 is provided in source for 25-channel arrangement.

INPUTS:    EEG_data: matrix of channels x timepoints
           Fs: data sampling frequency
           varargin{1}: figure title
           varargin{2}: scaling level, if not set will default to the range of the plotted data
           varargin{3}: duration of EEG to show in single window (in seconds)

Kirubin Pillay 
Department of Paediatrics, University of Oxford, Oxford, UK.
24/09/2019
%}

function plot_EEG_mat(EEGdata, Fs, varargin)

if length(varargin)==1
    title=varargin{1};
    spacing=0;
    winlength=5;
end

if length(varargin)==2
    if ~isempty(varargin{1})
        title=varargin{1};
    else
        title='EEG plot';
    end        
    spacing=varargin{2};
    winlength=5;
end

if length(varargin)==3
    if ~isempty(varargin{1})
        title=varargin{1};
    else
        title='EEG plot';
    end      
    if ~isempty(varargin{2})
        spacing=varargin{2};        
    else
        spacing=0;  
    end     
    winlength=varargin{3};
end

EEGdata=EEGdata.*-1;
eegplot(EEGdata,'srate', Fs,'title',title,'spacing',spacing,'winlength',winlength);



