#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Thie script loads in pre-processed data from the MATLAB function cluster_forpython.m and runs temporal clustering
(two tailed test, single channel) using the MNE toolbox.

This script uses the function 'mne.stats.spatio_temporal_cluster_1samp_test'.
See website for details: https://mne.tools/stable/generated/mne.stats.spatio_temporal_cluster_1samp_test.html

NOTE: This script does not take into account any trigger delays (such as if a PinPrick stimulus is used).
The cluster indices will need to be corrected for outside of this script.

@author: kirubinpillay
Department of Paediatrics, University of Oxford, Oxford, UK.
24/09/2019
"""

import numpy as np
import scipy as sp
import scipy.io as sio
import deepdish as dd
import mne

#  SET INPUTS
filepath = ''       # Filepath of data
stim_type = 'v'     # v=visual, t=tactile, a=auditory, pp=PinPrick (noxious)
n_perm = 1000       # Number of permutations to run
alpha = 0.025       # Alpha value for statistical significance
num_cut = 1         # Length of EEG to include (in s)
Fs = 2000           # Sampling frequency (Hz)

if stim_type == 'v':
    chann_num = 22                                                  # Select channel number from loaded data
    spatial_exclude = list(range(0, 22)) + list(range(23, 25))      # Exclude all channels except channel of interest
elif stim_type == 't':
    chann_num = 10
    spatial_exclude = list(range(0, 10)) + list(range(11, 25))
elif stim_type == 'a':
    chann_num = 10
    spatial_exclude = list(range(0, 10)) + list(range(11, 25))
elif stim_type == 'pp':
    spatial_exclude = list(range(0, 10)) + list(range(11, 25))

diffstim_cut = diffstim_all[:, 0:num_cut, :]

# Load data
filename = filepath + 'cluster_EEGdata_v1.0_' + stim_type + '.mat'
d = dd.io.load(filename)
adj = d['adj']
adj = sp.sparse.csr_matrix(adj)
diffstim_all = d['diffstim_all']
poststim_all = d['poststim_all']
prestim_all = d['prestim_all']
num_cut = num_cut * Fs

# Set clustering parameters
seed = 1    # Set random seed
tail = 0    # Two tailed test

# Perform cluster analysis
[t_obs, clusters, cluster_pv, H0] = mne.stats.spatio_temporal_cluster_1samp_test(diffstim_cut, connectivity=adj, n_permutations=n_perm, tail=tail,
                                                                                 seed=seed, spatial_exclude=spatial_exclude, step_down_p=0.05)

# Determine location of significant clusters
clusts_sig = np.where(cluster_pv < alpha)[0]
clusts_pv_sig = cluster_pv[clusts_sig]

# Save dictionary of results
d = np.zeros((len(clusts_sig)+1,), dtype=np.object)
d[0] = {'diff': diffstim_all,               # Save original data in dictionary
        'post': poststim_all,
        'pre': prestim_all,
        'pvals': clusts_pv_sig,             # Significant cluster p-values
        'tail': tail,                       # Store statistical test parameters
        'alpha': alpha,
        'clusters_all': clusters,           # Save cluster data
        'clusters_pv': cluster_pv}          # Save all cluster p-values

# Save cluster data
sio.savemat(filepath + 'cluster_results_' + stim_type + '.mat', {'d': d})
